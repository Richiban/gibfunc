﻿using System;
using System.Linq;

using NullGuard;

namespace GibFunc
{
    public abstract class Option<T>
    {
        public abstract TReturn MatchWith<TReturn>(Func<TReturn> none, Func<T, TReturn> some);

        public abstract void MatchWith(Action none = null, Action<T> some = null);

        public abstract bool HasValue
        {
            get;
        }

        public abstract T Value
        {
            get;
        }

        public static implicit operator Option<T>([AllowNull] T obj)
        {
            if (typeof(T).IsValueType)
                return new Some<T>(obj);

            return obj == null
                ? (Option<T>) new None<T>()
                : new Some<T>(obj);
        }

        private class Some<T> : Option<T>
        {
            private readonly T _value;

            public Some(T value)
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                _value = value;
            }

            public override TReturn MatchWith<TReturn>(Func<TReturn> none, Func<T, TReturn> some)
            {
                return some(_value);
            }

            public override void MatchWith(Action none = null, Action<T> some = null)
            {
                if (some != null)
                    some(_value);
            }

            public override bool HasValue
            {
                get
                {
                    return true;
                }
            }

            public override T Value
            {
                get
                {
                    return _value;
                }
            }
        }

        private class None<T> : Option<T>
        {
            public override TReturn MatchWith<TReturn>(Func<TReturn> none, Func<T, TReturn> some)
            {
                return none();
            }

            public override void MatchWith(Action none = null, Action<T> some = null)
            {
                if (none != null)
                    none();
            }

            public override bool HasValue
            {
                get
                {
                    return false;
                }
            }

            public override T Value
            {
                get
                {
                    throw new InvalidOperationException("Cannot get value of None");
                }
            }
        }
    }
}
